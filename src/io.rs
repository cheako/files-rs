use tokio::io::AsyncReadExt;

pub(crate) async fn read_line(stream: &mut tokio::net::TcpStream) -> Option<String> {
    let mut character = 0u8;
    let mut buffer = vec![];
    while character != b'\n' {
        character = stream.read_u8().await.ok()?;
        buffer.push(character);
        if buffer.len() > 128 {
            return None;
        };
    }
    let ret = std::str::from_utf8(&buffer)
        .ok()?
        .trim_end_matches(|c| c == '\r' || c == '\n');
    Some(ret.to_owned())
}
