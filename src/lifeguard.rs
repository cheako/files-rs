use std::borrow::Borrow;
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::convert::{AsMut, AsRef};
use std::fmt;
use std::future::Future;
use std::hash::{Hash, Hasher};
use std::mem::MaybeUninit;
use std::ops::{Deref, DerefMut, Drop};
use std::pin::Pin;
// use std::sync::{Mutex, MutexGuard};
use async_std::sync::{Mutex, MutexGuard};
use std::task::{Context, Poll};
// #[allow(dead_code)]

/// In order to be managed by a `Pool`, values must be of a type that
/// implements the `Recycleable` trait. This allows the `Pool` to create
/// new instances as well as reset existing instances to a like-new state.
pub trait Recycleable: Default + Future + Send {}

/// A smartpointer which uses a shared reference (`&`) to know
/// when to move its wrapped value back to the `Pool` that
/// issued it.
pub struct Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    value: RecycledInner<&'a Mutex<Pin<Box<CappedCollection<T>>>>, T>,
}

impl<'a, T> futures_core::Stream for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin + futures_core::Stream,
{
    type Item = <<T as core::future::Future>::Output as futures_core::Stream>::Item;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        Pin::new(self.value.as_mut()).poll_next(cx)
    }
}

impl<'a, T> AsRef<T::Output> for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    /// Gets a shared reference to the value wrapped by the smartpointer.
    fn as_ref(&self) -> &T::Output {
        self.value.as_ref()
    }
}

impl<'a, T> AsMut<T::Output> for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    /// Gets a mutable reference to the value wrapped by the smartpointer.
    fn as_mut(&mut self) -> &mut T::Output {
        self.value.as_mut()
    }
}

impl<'a, T> fmt::Debug for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: fmt::Debug + Unpin,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.value.fmt(f)
    }
}

impl<'a, T> fmt::Display for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: fmt::Display + Unpin,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.value.fmt(f)
    }
}

//-------- Passthrough trait implementations -----------

impl<'a, T> PartialEq for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: PartialEq + Unpin,
{
    fn eq(&self, other: &Self) -> bool {
        self.value.eq(&other.value)
    }
}

impl<'a, T> Eq for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Eq + Unpin,
{
}

impl<'a, T> PartialOrd for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: PartialOrd + Unpin,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value.partial_cmp(&other.value)
    }
}

impl<'a, T> Ord for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Ord + Unpin,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.cmp(&other.value)
    }
}

impl<'a, T> Hash for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Hash + Unpin,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.value.hash(state)
    }
}

//------------------------------------------------------

impl<'a, T> Deref for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    type Target = T::Output;
    #[inline]
    fn deref(&self) -> &T::Output {
        self.as_ref()
    }
}

impl<'a, T> DerefMut for Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    #[inline]
    fn deref_mut(&mut self) -> &mut T::Output {
        self.as_mut()
    }
}

impl<'a, T> Recycled<'a, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    #[allow(dead_code)]
    fn new(pool: &'a Mutex<Pin<Box<CappedCollection<T>>>>, value: T::Output) -> Recycled<'a, T> {
        Recycled {
            value: RecycledInner::new(pool, value),
        }
    }

    #[inline]
    /// Disassociates the value from the `Pool` that issued it. This
    /// destroys the smartpointer and returns the previously wrapped value.
    pub fn detach(self) -> T::Output {
        self.value.detach()
    }
}

struct RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    value: MaybeUninit<T::Output>,
    pool: MaybeUninit<P>,
}

// ---------- Passthrough Trait Implementations ------------

impl<P, T> PartialEq for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: PartialEq + Unpin,
{
    fn eq(&self, other: &Self) -> bool {
        unsafe { self.value.get_ref() }.eq(unsafe { other.value.get_ref() })
    }
}

impl<P, T> Eq for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Eq + Unpin,
{
}

impl<P, T> PartialOrd for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: PartialOrd + Unpin,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        unsafe { self.value.get_ref() }.partial_cmp(unsafe { other.value.get_ref() })
    }
}

impl<P, T> Ord for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Ord + Unpin,
{
    fn cmp(&self, other: &Self) -> Ordering {
        unsafe { self.value.get_ref() }.cmp(unsafe { other.value.get_ref() })
    }
}

impl<P, T> Hash for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Hash + Unpin,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        unsafe { self.value.get_ref() }.hash(state)
    }
}

// -------------------------------------------------------------

impl<P, T> Drop for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    #[inline]
    fn drop(&mut self) {
        let value = mem::replace(&mut self.value, MaybeUninit::uninit());
        let value = unsafe { value.assume_init() };
        let pool_ref = unsafe { self.pool.get_ref() }.borrow();
        if let Some(mut pool) = pool_ref.try_lock() {
            if pool.is_full() {
                drop(value);
                return;
            }
            pool.as_mut().values.push(value);
        }
    }
}

impl<P, T> AsRef<T::Output> for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    fn as_ref(&self) -> &T::Output {
        unsafe { self.value.get_ref() }
    }
}

impl<P, T> AsMut<T::Output> for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    fn as_mut(&mut self) -> &mut T::Output {
        unsafe { self.value.get_mut() }
    }
}

impl<P, T> fmt::Debug for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: fmt::Debug + Recycleable + Unpin,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        unsafe { self.value.get_ref() }.fmt(f)
    }
}

impl<P, T> fmt::Display for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: fmt::Display + Recycleable + Unpin,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        unsafe { self.value.get_ref() }.fmt(f)
    }
}

impl<P, T> Deref for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    type Target = T::Output;
    #[inline]
    fn deref(&self) -> &T::Output {
        self.as_ref()
    }
}

impl<P, T> DerefMut for RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    #[inline]
    fn deref_mut(&mut self) -> &mut T::Output {
        self.as_mut()
    }
}

impl<P, T> RecycledInner<P, T>
where
    P: Borrow<Mutex<Pin<Box<CappedCollection<T>>>>>,
    T: Recycleable,
    T::Output: Unpin,
{
    #[inline]
    fn new(pool: P, value: T::Output) -> RecycledInner<P, T> {
        RecycledInner {
            value: MaybeUninit::new(value),
            pool: MaybeUninit::new(pool),
        }
    }
    #[inline]
    fn detach(mut self) -> T::Output {
        let value = mem::replace(&mut self.value, MaybeUninit::uninit());
        let pool = mem::replace(&mut self.pool, MaybeUninit::uninit());
        mem::forget(self);
        drop(pool);
        unsafe { value.assume_init() }
    }
}

struct CappedCollection<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    futures: Vec<Pin<Box<T>>>,
    values: Vec<T::Output>,
    allocation_size: usize,
    max_size: usize,
}

impl<T> Future for CappedCollection<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    type Output = T::Output;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut values = vec![];
        let mut futures = vec![];
        futures.append(&mut self.futures);

        for mut future in futures.into_iter() {
            if let std::task::Poll::Ready(item) = future.as_mut().poll(cx) {
                // drop(future); // Shouldn't have to specify it.
                values.push(item);
            } else {
                self.futures.push(future);
            }
        }
        self.values.append(&mut values);
        match self.remove() {
            None => Poll::Pending,
            Some(r) => Poll::Ready(r),
        }
    }
}

impl<T> CappedCollection<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    #[inline]
    fn new(starting_size: usize, allocation_size: usize, max_size: usize) -> Self {
        use std::cmp;
        let starting_size = cmp::min(starting_size, max_size);
        let futures: Vec<_> = (0..starting_size).map(|_| Box::pin(T::default())).collect();
        Self {
            futures,
            values: vec![],
            allocation_size,
            max_size,
        }
    }

    #[inline]
    fn remove(&mut self) -> Option<T::Output> {
        self.values.pop()
    }

    #[inline]
    fn is_full(&self) -> bool {
        self.values.len() + self.futures.len() >= self.max_size
    }
    #[inline]
    fn len(&self) -> usize {
        self.values.len() + self.futures.len()
    }
    #[inline]
    fn allocation_size(&self) -> usize {
        self.allocation_size
    }
    #[inline]
    fn max_size(&self) -> usize {
        self.max_size
    }
}

/// A collection of values that can be reused without requiring new allocations.
///
/// `Pool` issues each value wrapped in a smartpointer. When the smartpointer goes out of
/// scope, the wrapped value is automatically returned to the pool.
pub struct Pool<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    values: Mutex<Pin<Box<CappedCollection<T>>>>,
}

impl<T> Pool<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    /// Returns the number of values remaining in the pool.
    #[inline]
    pub async fn size(&self) -> usize {
        self.values.lock().await.len()
    }
    #[inline]
    pub async fn allocation_size(&self) -> usize {
        self.values.lock().await.allocation_size()
    }
    /// Returns the maximum number of values the pool can hold.
    #[inline]
    pub async fn max_size(&self) -> usize {
        self.values.lock().await.max_size()
    }

    /// Removes a value from the pool and returns it wrapped in
    /// a `Recycled smartpointer. If the pool is empty when the
    /// method is called, a new value will be allocated.
    /* #[inline]
     * pub async fn new<'a>(&self) -> Option<Recycled<'a, T>> {
     *     let t = self.detached().await?;
     *     Some(Recycled::<'a, T> {
     *         value: RecycledInner::new(&self.values, t),
     *     })
     * }
     */
    /// Associates the provided value with the pool by wrapping it in a
    /// `Recycled` smartpointer.
    #[inline]
    pub fn attach(&self, value: T::Output) -> Recycled<T> {
        Recycled {
            value: RecycledInner::new(&self.values, value),
        }
    }

    /// Removes a value from the pool and returns it without wrapping it in
    /// a smartpointer. When the value goes out of scope it will not be
    /// returned to the pool.
    #[inline]
    pub async fn detached(&self) -> Option<T::Output> {
        let mut collection = self.values.lock().await;
        collection.as_mut().remove()
    }
}

/*
 * impl<'a, T> Future for Pool<T>
 * where
 *     T: Recycleable,
 * {
 *     type Output = Recycled<'a, T>;
 *
 *     fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
 *         self.values
 *             .lock()
 *             .unwrap()
 *             .as_mut()
 *             .poll(cx)
 *             .map(|value| Recycled::<T> {
 *                 value: RecycledInner::new(&self.values, value),
 *             })
 *     }
 * }
 */

pub(crate) async fn get_value<T>(p: &Pool<T>) -> Recycled<'_, T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    let value = p.values.lock().await.as_mut().await;
    Recycled::<T> {
        value: RecycledInner::new(&p.values, value),
    }
}

pub struct PoolGuard<'a, T: Recycleable>(MutexGuard<'a, Pin<Box<CappedCollection<T>>>>)
where
    T::Output: Unpin;

impl<T> Pool<T>
where
    T: Recycleable,
    T::Output: Unpin,
{
    pub async fn lock(&self) -> PoolGuard<'_, T> {
        PoolGuard(self.values.lock().await)
    }
}

/// Produces a `PoolBuilder` instance
///
/// # Example
///
/// ```
/// extern crate lifeguard;
/// use lifeguard::*;
///
/// fn main() {
///   let mut pool: Pool<String> = pool()
///     .with(StartingSize(128))
///     .with(MaxSize(4096))
///     .build();
/// }
/// ```
pub fn pool() -> PoolBuilder {
    PoolBuilder {
        starting_size: 16,
        allocation_size: 8,
        max_size: usize::max_value(),
    }
}

/// Used to define settings for and ultimately create a `Pool`.
pub struct PoolBuilder {
    pub starting_size: usize,
    pub allocation_size: usize,
    pub max_size: usize,
}

impl PoolBuilder {
    pub fn with<U>(self, option_setter: U) -> PoolBuilder
    where
        U: OptionSetter<PoolBuilder>,
    {
        option_setter.set_option(self)
    }

    pub fn build<T>(self) -> Pool<T>
    where
        T: Recycleable,
        T::Output: Unpin,
    {
        let values: CappedCollection<T> =
            CappedCollection::new(self.starting_size, self.allocation_size, self.max_size);
        Pool {
            values: Mutex::from(Box::pin(values)),
        }
    }
}

pub mod settings {
    use super::PoolBuilder;
    /// Implementing this trait allows a struct to act as a configuration
    /// parameter in the builder API.
    pub trait OptionSetter<T> {
        fn set_option(self, option: T) -> T;
    }

    /// Specifies how many values should be requested from the Supplier at
    /// initialization time. These values will be available for immediate use.
    pub struct StartingSize(pub usize);
    pub struct AllocationSize(pub usize);
    /// Specifies the largest number of values the `Pool` will hold before it
    /// will begin to drop values being returned to it.
    pub struct MaxSize(pub usize);
    impl OptionSetter<PoolBuilder> for StartingSize {
        fn set_option(self, mut builder: PoolBuilder) -> PoolBuilder {
            let StartingSize(size) = self;
            builder.starting_size = size;
            builder
        }
    }
    impl OptionSetter<PoolBuilder> for AllocationSize {
        fn set_option(self, mut builder: PoolBuilder) -> PoolBuilder {
            let AllocationSize(size) = self;
            builder.allocation_size = size;
            builder
        }
    }
    impl OptionSetter<PoolBuilder> for MaxSize {
        fn set_option(self, mut builder: PoolBuilder) -> PoolBuilder {
            let MaxSize(size) = self;
            builder.max_size = size;
            builder
        }
    }
}

pub use settings::{MaxSize, OptionSetter, StartingSize};
use std::mem;
