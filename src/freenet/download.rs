use std::time::Duration;

use warp::reject;

use super::*;

#[derive(Debug, derive_builder::Builder)]
struct ReqDataFound {
    completion_time: Duration,
    startup_time: Duration,
    data_length: usize,
    global: bool,
    content_type: String,
}

pub(crate) async fn file<'a>(
    file: crate::file_get::Request,
    mut name: String,
) -> Result<http::response::Response<hyper::Body>, reject::Rejection> {
    use tokio::io::AsyncWriteExt;
    name.retain(|character| !character.is_ascii_control());
    let mut fapi = crate::freenet::get_fapi().await;
    let identifier = fapi.get_identifier();
    let mut socket = fapi.stream.as_mut();
    let buffer = format!(
        "ClientGet
URI={}/{}
Identifier={}
EndMessage
",
        file.to_path(),
        name,
        identifier,
    )
    .into_bytes();
    let two = format!("Identifier={}", identifier);
    assert_eq!(socket.write(&buffer).await.unwrap(), buffer.len());
    loop {
        use crate::io::read_line;
        match read_line(&mut socket).await.as_ref().map(|s| s.as_str()) {
            Some("") | None => {}
            Some("DataFound") => {
                let mut data_found_builder = ReqDataFoundBuilder::default();
                loop {
                    match read_line(&mut socket).await.as_ref().map(|s| s.as_str()) {
                        Some("") | None => {}
                        Some(line) if line == two.as_str() => {}
                        Some("EndMessage") => {
                            let mut acc = "".to_owned();
                            let df = data_found_builder.build().unwrap();
                            for _ in 0u8..8 {
                                read_line(&mut socket)
                                    .await
                                    .as_ref()
                                    .map(|s| {
                                        let mut s = s.clone();
                                        s.push('\n');
                                        acc.push_str(s.as_str());
                                    })
                                    .unwrap()
                            }
                            assert_eq!(
                                acc,
                                format!(
                                    "AllData
Identifier={}
CompletionTime={}
StartupTime={}
DataLength={}
Global={}
Metadata.ContentType={}
Data
",
                                    identifier,
                                    df.completion_time.as_secs(),
                                    df.startup_time.as_secs(),
                                    df.data_length,
                                    if df.global { "true" } else { "false" },
                                    df.content_type,
                                )
                                .as_str()
                            );
                            fapi.bytes_left = std::num::NonZeroUsize::new(df.data_length);
                            use http::header::*;
                            let response = http::response::Response::builder()
                                .header(
                                    CONTENT_TYPE,
                                    HeaderValue::from_str(df.content_type.as_str()).unwrap(),
                                )
                                .header(
                                    CONTENT_LENGTH,
                                    HeaderValue::from_str(df.data_length.to_string().as_str())
                                        .unwrap(),
                                )
                                .body(hyper::body::Body::wrap_stream(fapi))
                                .unwrap();
                            return Ok(response);
                        }
                        Some(line) => {
                            lazy_static! {
                                static ref COMPLETION_TIME: Regex =
                                    Regex::new("^CompletionTime=([0-9]+)$").unwrap();
                                static ref STARTUP_TIME: Regex =
                                    Regex::new("^StartupTime=([0-9]+)$").unwrap();
                                static ref DATA_LENGTH: Regex =
                                    Regex::new("^DataLength=([0-9]+)$").unwrap();
                                static ref GLOBAL: Regex =
                                    Regex::new("^Global=(false|true)$").unwrap();
                                static ref CONTENT_TYPE: Regex =
                                    Regex::new("^Metadata.ContentType=(.+)$").unwrap();
                            }
                            if let Some(c) = COMPLETION_TIME.captures(line) {
                                data_found_builder
                                    .completion_time(Duration::from_secs(c[1].parse().unwrap()));
                            } else if let Some(c) = STARTUP_TIME.captures(line) {
                                data_found_builder
                                    .startup_time(Duration::from_secs(c[1].parse().unwrap()));
                            } else if let Some(c) = DATA_LENGTH.captures(line) {
                                data_found_builder.data_length(c[1].parse().unwrap());
                            } else if let Some(c) = GLOBAL.captures(line) {
                                data_found_builder.global(&c[1] == "true");
                            } else if let Some(c) = CONTENT_TYPE.captures(line) {
                                data_found_builder.content_type(c[1].to_owned());
                            } else {
                                #[derive(Debug)]
                                struct NotDataFoundCommand(String);
                                impl reject::Reject for NotDataFoundCommand {}
                                return Err(reject::custom(NotDataFoundCommand(line.to_owned())));
                            }
                        }
                    }
                }
            }
            Some(line) => {
                #[derive(Debug)]
                struct NotDataFound(String);
                impl reject::Reject for NotDataFound {}
                return Err(reject::custom(NotDataFound(line.to_owned())));
            }
        };
    }
}
