#![feature(maybe_uninit_ref)]
// #![deny(warnings)]

use tokio::net::UnixListener;
use warp::Filter;

mod file_get;
mod freenet;
mod io;
pub mod lifeguard;
mod temputil;

#[tokio::main]
async fn main() -> Result<(), ()> {
    use std::ffi::CString;
    use std::fs;
    use std::os::unix::fs::PermissionsExt;
    const DIR: &str = "/home/srv_files/var";
    #[cfg(debug_assertions)]
    const FILE: &str = "/home/srv_files/var/warp-dev.sock";
    #[cfg(not(debug_assertions))]
    const FILE: &str = "/home/srv_files/var/warp.sock";
    const GRP: &str = "www-data";

    pretty_env_logger::init();

    let file_downloads = warp::path::param()
        .and(warp::path::param())
        .and_then(freenet::download::file);

    let routes = file_downloads;

    let gid = {
        let grp = CString::new(GRP).unwrap();
        unsafe { libc::getgrnam(grp.as_ptr()).as_ref() }
            .unwrap()
            .gr_gid
    };

    // static mut LISTENER: Option<UnixListener> = None;
    let mut listener = {
        use std::ffi::OsStr;
        temputil::create_helper(
            &std::path::Path::new(OsStr::new(DIR)),
            &OsStr::new(""),
            &OsStr::new(""),
            6,
            |temp_path| {
                use std::io;

                #[doc(hidden)]
                pub trait IsMinusOne {
                    fn is_minus_one(&self) -> bool;
                }

                macro_rules! impl_is_minus_one {
    ($($t:ident)*) => ($(impl IsMinusOne for $t {
        fn is_minus_one(&self) -> bool {
            *self == -1
        }
    })*)
}

                impl_is_minus_one! { i8 i16 i32 i64 isize }
                fn cvt<T: IsMinusOne>(t: T) -> io::Result<T> {
                    if t.is_minus_one() {
                        Err(io::Error::last_os_error())
                    } else {
                        Ok(t)
                    }
                }

                let listener = UnixListener::bind(temp_path.clone())?;
                let path = CString::new(temp_path.clone().to_str().unwrap()).unwrap();
                cvt(unsafe { libc::chown(path.as_ptr(), std::u32::MAX, gid) })?;
                fs::set_permissions(temp_path.clone(), PermissionsExt::from_mode(0o660))?;
                fs::rename(temp_path, FILE)?;
                Ok(listener)
            },
        )
        .unwrap()
    };

    let incoming = listener.incoming();
    warp::serve(routes).run_incoming(incoming).await;
    Ok(())
}
