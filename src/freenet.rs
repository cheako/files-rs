use bigint::uint::U128;
use std::future::Future;
use std::num::NonZeroUsize;
use std::pin::Pin;
use std::sync::atomic;
use std::task::{Context, Poll};

pub(crate) mod download;
pub(crate) mod upload;

use lazy_static::lazy_static;
use regex::Regex;

use hyper::body::Bytes;
use tokio::net::TcpStream;

use crate::lifeguard;

#[derive(Debug, derive_builder::Builder)]
pub(crate) struct FapiProps {
    pub(crate) revision: String,
    pub(crate) testnet: bool,
    pub(crate) version: String,
    pub(crate) fbuild: u32,
    pub(crate) id: U128,
    pub(crate) node: String,
    pub(crate) ebuild: u32,
    pub(crate) fcp_version: String,
    pub(crate) node_language: String,
    pub(crate) ext_revision: String,
}

pub(crate) struct Fapi {
    pub(crate) stream: Pin<Box<TcpStream>>,
    #[allow(dead_code)]
    pub(crate) props: FapiProps,
    identifier: atomic::AtomicU64,
    pub(crate) bytes_left: Option<NonZeroUsize>,
}

impl Fapi {
    pub(crate) fn get_identifier(&self) -> u64 {
        self.identifier.fetch_add(1, atomic::Ordering::Relaxed)
    }
}

impl futures_core::Stream for Fapi {
    type Item = Result<Bytes, Box<dyn std::error::Error + 'static + Send + Sync>>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        use Poll::*;
        if let Some(num_bytes) = self.bytes_left {
            use std::mem::{self, MaybeUninit};
            use tokio::io::AsyncRead;
            let buffer: Vec<MaybeUninit<u8>> = (0..num_bytes.get())
                .map(|_| unsafe { MaybeUninit::uninit().assume_init() })
                .collect();
            #[allow(clippy::unsound_collection_transmute)]
            let mut buffer = unsafe { mem::transmute::<_, Vec<u8>>(buffer) };
            match self.stream.as_mut().poll_read(cx, &mut buffer) {
                Ready(result) => match result {
                    Ok(0) => Pending,
                    Ok(size) => {
                        buffer.truncate(size);
                        let bytes_remaining = num_bytes.get() - size;
                        self.bytes_left = if bytes_remaining != 0 {
                            Some(NonZeroUsize::new(bytes_remaining).unwrap())
                        } else {
                            None
                        };
                        Ready(Some(Ok(Bytes::from(buffer))))
                    }
                    Err(e) => Ready(Some(Err(Box::new(e)))),
                },
                Pending => Pending,
            }
        } else {
            Ready(None)
        }
    }
}

pub(crate) struct FapiFuture(Pin<Box<dyn Future<Output = Fapi> + Send>>);

impl Default for FapiFuture {
    fn default() -> Self {
        let fut: Pin<Box<dyn Future<Output = Fapi> + Send>> = Box::pin(new_fapi());
        Self(fut)
    }
}

impl Future for FapiFuture {
    type Output = Fapi;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        self.0.as_mut().poll(cx)
    }
}

async fn new_fapi() -> Fapi {
    loop {
        use rand::distributions::Alphanumeric;
        use rand::Rng;
        use tokio::io::AsyncWriteExt;
        let mut stream =
            match TcpStream::connect((std::net::Ipv4Addr::new(127, 0, 0, 1), 9481)).await {
                Err(e) => {
                    /* Todo: only about 15/min then panic */
                    println!("TcpStream::connect Err: {}", e);
                    continue;
                }
                Ok(s) => s,
            };
        let helo = format!(
            "ClientHello
Name=Warp Files:{}
ExpectedVersion=2.0
EndMessage
",
            {
                let mut buf = "".to_owned();
                rand::thread_rng()
                    .sample_iter(&Alphanumeric)
                    .take(8)
                    .for_each(|b| {
                        buf.push_str(unsafe { std::str::from_utf8_unchecked(&[b as u8]) })
                    });
                buf
            }
        );
        assert_eq!(
            stream.write(helo.as_bytes()).await.unwrap(),
            helo.as_bytes().len()
        );
        let mut props_builder: FapiPropsBuilder = Default::default();
        loop {
            use crate::io::read_line;
            match read_line(&mut stream).await.as_ref().map(|s| s.as_str()) {
                Some("") | None => {}
                Some("NodeHello") => loop {
                    match read_line(&mut stream).await.as_ref().map(|s| s.as_str()) {
                        Some("") | None => {}
                        Some("EndMessage") => {
                            let props = props_builder.build().unwrap();
                            if cfg!(debug_assertions) {
                                println!("{:?}", props);
                            }
                            return Fapi {
                                stream: Box::pin(stream),
                                props,
                                identifier: atomic::AtomicU64::new(0),
                                bytes_left: None,
                            };
                        }
                        Some(line) if line.starts_with("CompressionCodecs=") => {}
                        Some(line) => {
                            lazy_static! {
                                static ref REVISION: Regex = Regex::new("^Revision=(.*)$").unwrap();
                                static ref TESTNET: Regex =
                                    Regex::new("^Testnet=(false|true)$").unwrap();
                                static ref VERSION: Regex = Regex::new("^Version=(.*)$").unwrap();
                                static ref BUILD: Regex = Regex::new("^Build=([\\d]+)$").unwrap();
                                static ref CONNECTION_IDENTIFIER: Regex =
                                    Regex::new("^ConnectionIdentifier=([[:xdigit:]]{32})$")
                                        .unwrap();
                                static ref NODE: Regex = Regex::new("^Node=(.*)$").unwrap();
                                static ref EXT_BUILD: Regex =
                                    Regex::new("^ExtBuild=([\\d]+)$").unwrap();
                                static ref FCP_VERSION: Regex =
                                    Regex::new("^FCPVersion=(.*)$").unwrap();
                                static ref NODE_LANGUAGE: Regex =
                                    Regex::new("^NodeLanguage=(.*)$").unwrap();
                                static ref EXT_REVISION: Regex =
                                    Regex::new("^ExtRevision=(.*)$").unwrap();
                            }
                            if let Some(c) = REVISION.captures(line) {
                                props_builder.revision(c[1].to_owned());
                            } else if let Some(c) = TESTNET.captures(line) {
                                props_builder.testnet(&c[1] == "true");
                            } else if let Some(c) = VERSION.captures(line) {
                                props_builder.version(c[1].to_owned());
                            } else if let Some(c) = BUILD.captures(line) {
                                props_builder.fbuild(c[1].parse().unwrap());
                            } else if let Some(c) = CONNECTION_IDENTIFIER.captures(line) {
                                props_builder
                                    .id(U128::from(hex::decode(&c[1]).unwrap().as_slice()));
                            } else if let Some(c) = NODE.captures(line) {
                                props_builder.node(c[1].to_owned());
                            } else if let Some(c) = EXT_BUILD.captures(line) {
                                props_builder.ebuild(c[1].parse().unwrap());
                            } else if let Some(c) = FCP_VERSION.captures(line) {
                                props_builder.fcp_version(c[1].to_owned());
                            } else if let Some(c) = NODE_LANGUAGE.captures(line) {
                                props_builder.node_language(c[1].to_owned());
                            } else if let Some(c) = EXT_REVISION.captures(line) {
                                props_builder.ext_revision(c[1].to_owned());
                            } else {
                                unimplemented!("Unknown helo command: {}", line);
                            }
                        }
                    }
                },
                Some(line) => unimplemented!("Unknown command: {}", line),
            };
        }
        /*
        match self.0.lock().unwrap().poll() {
            Ok(Ready(r)) if r.status() != 301 => {
                println!("!301 {:?}", r);
                Err(reject::not_found())
            }
            Ok(Ready(r)) => {
                println!("{} {:?}", r.status(), r);
                Ok(Ready(r))
            }
            r => {
                println!("Other {:?}", r);
                r.map_err(|e| reject::custom(e))
            }
        }
         */
    }
}

impl lifeguard::Recycleable for FapiFuture {}

pub(crate) async fn get_fapi<'a>() -> lifeguard::Recycled<'a, FapiFuture> {
    use lifeguard::*;
    lazy_static! {
        static ref FAPI: Pool<FapiFuture> = pool().build();
    };
    get_value(&FAPI).await
}
