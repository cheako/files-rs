use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
pub(crate) enum Request {
    CHK(String, String, String),
    SSK(String, String, String),
    USK(String, String, String),
}
use Request::*;

impl FromStr for Request {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                "^(?P<t>CHK|SSK|USK)@(?P<h>[A-Za-z0-9~-]+),(?P<k>[A-Za-z0-9~-]+)(:?,(?P<s>[A-Za-z0-9~-]+={0,2}))?$"
            )
            .unwrap();
        }
        if let Some(caps) = RE.captures(s) {
            match caps.name("t").ok_or(())?.as_str() {
                "CHK" => Ok(CHK(
                    caps.name("h").ok_or(())?.as_str().to_owned(),
                    caps.name("k").ok_or(())?.as_str().to_owned(),
                    caps.name("s").map_or("", |s| s.as_str()).to_owned(),
                )),
                "SSK" => Ok(SSK(
                    caps.name("h").ok_or(())?.as_str().to_owned(),
                    caps.name("k").ok_or(())?.as_str().to_owned(),
                    caps.name("s").map_or("", |s| s.as_str()).to_owned(),
                )),
                "USK" => Ok(USK(
                    caps.name("h").ok_or(())?.as_str().to_owned(),
                    caps.name("k").ok_or(())?.as_str().to_owned(),
                    caps.name("s").map_or("", |s| s.as_str()).to_owned(),
                )),
                _ => Err(()),
            }
        } else {
            Err(())
        }
    }
}

impl Request {
    pub fn to_path(&self) -> String {
        match self {
            CHK(h, k, s) => {
                if s.is_empty() {
                    format!("CHK@{},{}", h, k)
                } else {
                    format!("CHK@{},{},{}", h, k, s)
                }
            }
            SSK(h, k, s) => {
                if s.is_empty() {
                    format!("SSK@{},{}", h, k)
                } else {
                    format!("SSK@{},{},{}", h, k, s)
                }
            }
            USK(h, k, s) => {
                if s.is_empty() {
                    format!("USK@{},{}", h, k)
                } else {
                    format!("USK@{},{},{}", h, k, s)
                }
            }
        }
    }
}
